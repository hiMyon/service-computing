package rxgo_test

import (
	"testing"
	RxGo "github.com/pmlpml/rxgo"
	"github.com/stretchr/testify/assert"
)

func fibonacci(max int) func() (int, bool) {
	a, b := 0, 1
	return func() (r int, end bool) {
		r = a
		a, b = b, a+b
		if r > max {
			end = true
		}
		return
	}
}

func TestDistinct (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).Distinct().Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {0,1,2,3,5,8}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestDebounce (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).Debounce(1).Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {8}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestElementAt (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).ElementAt(4).Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {3}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestFirst (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).First().Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {0}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestFind (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).Find(func (x int) bool {
		return x > 4
	}).Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {5}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestIgnoreElements (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).IgnoreElements().Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestLast (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).Last().Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {8}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestSkip (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).Skip(2).Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {1,2,3,5,8}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestSkipLast (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).SkipLast(2).Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {0,1,1,2,3}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestTake (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).Take(4).Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {0,1,1,2}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

func TestTakeLast (t *testing.T) {
	got := []int{}
	RxGo.Start(fibonacci(10)).TakeLast(4).Subscribe(func (x int) {
		got = append(got, x)
	})
	expected := []int {2,3,5,8}
	assert.Equal(t, expected, got, "Distinct Test Error! expected %v got %v\n", expected, got)
}

