package rxgo

import (
	"context"
	"reflect"
	"sync"
	"time"
)
// filter node implementation of streamOperator
type filterOperator struct {
	opFunc func(ctx context.Context, o *Observable, item reflect.Value, out chan interface{}) (end bool)
}

func (ftop filterOperator) op(ctx context.Context, o *Observable) {
	// must hold defintion of flow resourcs here, such as chan etc., that is allocated when connected
	// this resurces may be changed when operation routine is running.
	in := o.pred.outflow
	out := o.outflow
	//fmt.Println(o.name, "operator in/out chan ", in, out)
	var wg sync.WaitGroup

	go func() {
		end := false
		for x := range in {
			if end {
				continue
			}
			// can not pass a interface as parameter (pointer) to gorountion for it may change its value outside!
			xv := reflect.ValueOf(x)
			// send an error to stream if the flip not accept error
			if e, ok := x.(error); ok && !o.flip_accept_error {
				o.sendToFlow(ctx, e, out)
				continue
			}
			// scheduler
			switch threading := o.threading; threading {
			case ThreadingDefault:
				if ftop.opFunc(ctx, o, xv, out) {
					end = true
				}
			case ThreadingIO:
				fallthrough
			case ThreadingComputing:
				wg.Add(1)
				go func() {
					defer wg.Done()
					if ftop.opFunc(ctx, o, xv, out) {
						end = true
					}
				}()
			default:
			}
		}
		if o.all_elements != nil {
			for _, x := range o.all_elements {
				if !end {
					end = o.sendToFlow(ctx, x, out)
				}
			}
		}

		wg.Wait() //waiting all go-routines completed
		o.closeFlow(out)
	}()
}

func (parent *Observable) Distinct() (o *Observable) {
	o = parent.newFilterObservable("distinct")
	mp := make(map[interface{}] bool)
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			// send data
			if mp[x.Interface()] {
				return
			}
			if !end {
				mp[x.Interface()] = true
				end = o.sendToFlow(ctx, x.Interface(), out)
			}	
			return
		}}
	return o
}

func (parent *Observable) Debounce(during int) (o *Observable) {
	o = parent.newFilterObservable("debounce")
	o.threading = ThreadingComputing //每个数据都开一个go程
	cnt := 0
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			cnt++
			temp := cnt
			time.Sleep(time.Millisecond * time.Duration(during))
			if !end && temp == cnt {
				end = o.sendToFlow(ctx, x.Interface(), out)
			}	
			return
		}}
	return o
}

func (parent *Observable) ElementAt(index int) (o *Observable) {
	o = parent.newFilterObservable("elementAt")
	cnt := 0
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			if !end && cnt == index{
				o.sendToFlow(ctx, x.Interface(), out)
				end = true
			}	
			cnt++
			return
		}}
	return o
}

func (parent *Observable) First() (o *Observable) {
	return parent.ElementAt(0)
}


func (parent *Observable) Find(f interface{}) (o *Observable) {
	o = parent.newFilterObservable("find")
	fv := reflect.ValueOf(f)
	o.operator =  filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
		params := []reflect.Value{x}
		rs, skip, stop, e := userFuncCall(fv, params)
		var item interface{} = rs[0].Interface()
		if stop {
			end = true
			return
		}
		if skip {
			return
		}
		if e != nil {
			item = e
		}
		// send data
		if !end {
			if b, ok := item.(bool); ok && b {
				o.sendToFlow(ctx, x.Interface(), out)
				end = true
			}
		}
		return
	}}
	return o
}

func (parent *Observable) IgnoreElements() (o *Observable) {
	o = parent.newFilterObservable("ignoreElements")
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			return
		}}
	return o
}

func (parent *Observable) Last() (o *Observable) {
	o = parent.newFilterObservable("last")
	o.all_elements = []interface{}{}
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			if len(o.all_elements) == 0 {
				o.all_elements = append(o.all_elements, x.Interface())
			} else {
				o.all_elements[0] = x.Interface()
			}
			return
		}}
	return o
}

func (parent *Observable) Skip(n int) (o *Observable) {
	o = parent.newFilterObservable("skip")
	cnt := 0
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			cnt++
			if cnt > n {
				end = o.sendToFlow(ctx, x.Interface(), out)
			}
			return
		}}
	return o
}

func (parent *Observable) SkipLast(n int) (o *Observable) {
	o = parent.newFilterObservable("skipLast")
	temp := []interface{}{}
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			if len(temp) >= n {
				end = o.sendToFlow(ctx, temp[0], out)
				temp = append(temp[1:], x.Interface())
			} else {
				temp = append(temp, x.Interface())
			}
			return
		}}
	return o
}

func (parent *Observable) Take(n int) (o *Observable) {
	o = parent.newFilterObservable("take")
	cnt := 0
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			cnt++
			if cnt > n {
				return
			}
			
			end = o.sendToFlow(ctx, x.Interface(), out)	
			return
		}}
	return o
}

func (parent *Observable) TakeLast(n int) (o *Observable) {
	o = parent.newFilterObservable("takeLast")
	o.all_elements = []interface{}{}
	o.operator = filterOperator { func(ctx context.Context, o *Observable, x reflect.Value, out chan interface{}) (end bool) {
			if len(o.all_elements) < n {
				o.all_elements = append(o.all_elements, x.Interface())
			} else {
				o.all_elements = append(o.all_elements[1:], x.Interface())
			}
			return
		}}
	return o
}

func (parent *Observable) newFilterObservable(name string) (o *Observable) {
	//new Observable
	o = newObservable()
	o.Name = name

	//chain Observables
	parent.next = o
	o.pred = parent
	o.root = parent.root

	//set options
	o.buf_len = BufferLen
	return o
}