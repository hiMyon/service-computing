package ConfigurationReader

import (
	"testing"
	//"fmt"
)

func TestGetSection(t *testing.T) {
	cfg := NewIniCFG()
	sec := NewSection()
	sec.mp["hello"] = "world"
	cfg.mp["1"] = sec

	got, err := cfg.GetSection("1")
	if (err != nil) {
		t.Error(err)
	}
	got1 := got.GetValByKey("hello")
	if got1 != "world" {
		t.Errorf("want %s, got %s.",  "world", got1);
	}
	
}

func TestRead(t *testing.T) {
	filename := "data.ini"
	var listener MyListener

	cfg0, err := Watch(filename, listener)
	if err != nil {
		t.Error(err)
	}

	cfg := NewIniCFG()
// app_mode
	sec := NewSection()
	cfg.mp[""] = sec
	sec.mp["app_mode"] = "development"
	//不能直接比较两个map是否相同，即使内部完全相同，也会因为地址不同而不同
	if (cfg.mp[""].mp["app_mode"] != cfg0.mp[""].mp["app_mode"]) {
		t.Errorf("want %v, got %v.",  cfg.mp[""].mp["app_mode"], cfg0.mp[""].mp["app_mode"] );
	}

//paths
	sec1 := NewSection()
	cfg.mp["paths"] = sec1
	sec1.mp["data"] = "/home/git/grafana"
	if (cfg.mp["paths"].mp["data"] != cfg0.mp["paths"].mp["data"]) {
		t.Errorf("want %v, got %v.",  cfg.mp["paths"].mp["data"] , cfg0.mp["paths"].mp["data"] );
	}

//server
	sec2 := NewSection()
	cfg.mp["server"] = sec2
	sec2.mp["enforce_domain"] = "true"
	if (cfg.mp["server"].mp["enforce_domain"] != cfg0.mp["server"].mp["enforce_domain"]) {
		t.Errorf("want %v, got %v.",  cfg.mp["server"].mp["enforce_domain"] , cfg0.mp["server"].mp["enforce_domain"] );
	}

//全部打印出来，用于快速人工检查
	// for key, value := range cfg0.mp {
	// 	fmt.Print(key)
	// 	fmt.Print(value)
	// }
	// t.Error(1)

}

// func TestListen(t *testing.T) {

// }