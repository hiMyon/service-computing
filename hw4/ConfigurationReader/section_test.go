package ConfigurationReader

import (
	"testing"
)

func TestSection(t *testing.T) {
	sec := NewSection()
	sec.mp["hello"] = "world"

	got := sec.GetValByKey("hello");
	if got != "world" {
		t.Errorf("want %s, got %s.",  "world", got);
	}
	
}