package ConfigurationReader

import (
	"io/ioutil"
	"fmt"
	"os"
	//"bufio"
	"runtime"
	//"strings"
	//"time"
)
//目前只支持读取ini格式的配置文件

//全局变量
var (
	commentSymbol byte = '#'
	finishRead = false
	readCount = 0
)


//iniCFG 定义了配置文件内容的存储结构
type iniCFG struct {
	mp map[string]*Section
}

func NewIniCFG() *iniCFG {
	return &iniCFG {
		mp: make(map[string]*Section),
	}
}

func (cfg *iniCFG) GetSection(secName string) (*Section, error) {
	mpSecName, isPresent := cfg.mp[secName]
	if isPresent == false {
		return mpSecName, SecNameDoesNotExist {secName}
	}
	return mpSecName, nil
}

func init() {
	if runtime.GOOS == "windows" {
		commentSymbol = ';'
	}
}

type Listener interface {
	listen(filename string, ch chan int )
}

type MyListener struct {

}

//获取文件修改时间 返回unix时间戳
func GetFileModTime(path string) int64 {
	f, err := os.Open(path)
	if err != nil {
		// log.Println("open file error")
		// return time.Now().Unix()
		panic(err)
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		panic(err)
	}

	return fi.ModTime().Unix()
}

func (listener MyListener) listen(filename string, ch chan int ) {
	lastModTime := GetFileModTime(filename)
	for {
		
		modTime := GetFileModTime(filename)
		if (modTime != lastModTime) {
			lastModTime = modTime
			fmt.Print("fie changed, restart reading.\n")
			readCount++
			ch <- 1
			
		}
		if (finishRead) {
			if (readCount > 0) {//有新开的read 线程未结束
				readCount--
				finishRead = false
				continue
			}
			ch <- 0
			return
		}
	}
}



//Watch 读取ini配置文件，将信息存储在CFG结构中并返回
//listener 是监听器，在另一个go程中运行，如果读取文件过程中文件内容发生改变，则通过某种方式告知当前进程，当前进程根据情况作出反应（忽略或重读）
func Watch(filename string ,listener Listener) (*iniCFG, error) {
	ch := make(chan int)
	ch_cfg := make(chan *iniCFG)
	go listener.listen(filename, ch)
	go Read(filename, ch_cfg)
	
	for {
		ret := <-ch
		if (ret == 0) {
			return <-ch_cfg, nil
		} else if (ret == 1) {
			go Read(filename, ch_cfg)
		}
	}
	

}

func Read(filename string, ch_cfg chan *iniCFG) {
	//获取文件内容
	content, err := ioutil.ReadFile("data.ini")
    if err != nil {
        panic(err)
    }
	
	//fmt.Print(string(content))
	//readCount++
	//解析文件内容并存储在cfg中
	cfg := NewIniCFG()
	sec := NewSection()
	cfg.mp[""] = sec
	var key, value string
	var j int
	for i := 0; i < len(content);  {
		//下面一行只在测试时为了有充足时间修改文件才需要
		//time.Sleep(time.Duration(100) * time.Millisecond)
		ch := content[i]
		//如果是注释符号，则将整行忽略掉
		if ch == commentSymbol {
			for i < len(content) && content[i] != '\n' {
				i++
			}
		} else if ch == '[' {//如果是左中括号，则获取括号中的字符串作为段名，注意不能包括左右括号
			i++
			for i < len(content) && (content[i] == ' ' || content[i] == '\t' || content[i] == '\n') {
				i++
			}
			j = i
			for i < len(content) && !(content[i] == ' ' || content[i] == '\t' || content[i] == '\n' || content[i] == ']') {
				i++
			}
			secName := string(content[j:i])
			sec = NewSection()
			cfg.mp[secName] = sec

			// fmt.Print("hahahahaha")
			// fmt.Print(secName)

			for i < len(content) && content[i] != ']' {
				i++
			}
			i++
			
		} else if ch == ' ' || ch == '\t' || ch == '\n' { //如果是空格等，则跳过
			i++
		} else { //是键值对，则分别读取等号左侧和右侧字符串，并存入当前的段对应的字典中
			j = i
			for i < len(content) && !(content[i] == '='  || content[i] == '\t' || content[i] == ' ' || content[i] == '\n')  {
				i++
			}
			key = string(content[j:i])
			// fmt.Print("hahahahaha")
			// fmt.Print(key)
			for i < len(content) && content[i] != '=' {
				i++
			}
			i++
			for i < len(content) && (content[i] == ' ' || content[i] == '\t' || content[i] ==  '\n') {
				i++
			}
			j = i
			for i < len(content) && !(content[i] == ' ' || content[i] ==  '\t' || content[i] ==  '\n') {
				i++
			}
			value = string(content[j:i])
			sec.mp[key] = value
		}
		
	}
	finishRead = true
	//readCount--
	ch_cfg <- cfg
}

