package ConfigurationReader

// import (
// 	"fmt"
// )

//Section 定义了一个段落的内容
type Section struct {
	mp map[string]string
}
func (sec *Section) GetValByKey(key string) string {
	return sec.mp[key]
}

func NewSection() *Section {
	return &Section {
		mp: make(map[string]string),
	}
}