### Package api

#### *type Section*
```go
type Section struct {
    mp map[string]string
}
```

#### func (*Section) GetValByKey
```go
func (sec *Section) GetValByKey(key string) (string) {
    return sec.mp[key]
}
```

#### func  NewSection
```go
func NewSection() {
    return &Section {
		mp: make(map[string]string),
	}
}
```

#### *type iniCFG*
```go
type iniCFG struct {
    mp map[string]Section
}
```

#### func (*iniCFG) GetSection
```go
func (cfg *iniCFG) GetSection(secName string) (*Section, err) {
    mpSecName, isPresent := cfg.mp[secName]
	if isPresent == false {
		return mpSecName, SecNameDoesNotExist {secName}
	}
	return mpSecName, nil
}
```

#### func NewIniCFG
```go
func NewIniCFG() *iniCFG {
	return &iniCFG {
		mp: make(map[string]*Section),
	}
}
```

#### *type Listener*
```go
type Listener interface {
	listen(filename string, ch chan int )
}
```

#### *type MyListener*
```go
type MyListener struct {

}
```

#### func (MyListener) listen
```go
func (listener MyListener) listen(filename string, ch chan int )
```

#### func Read
```go
func Read(filename string, ch chan iniCFG)
```

#### func Watch
```go
func Watch(filename string ,listener Listener) (*iniCFG, error)
```

#### func init
```go
func init() {
	if runtime.GOOS == "windows" {
		commentSymbol = ';'
	}
}
```

#### func GetFileModTime
```go
func GetFileModTime(path string) (int64)
```

