### 这个包实现了对ini配置文件的带监听的简单解析，以下是一个使用它的例子。

假设要读取下面的文件
```bash
# possible values : production, development
app_mode = developments

[ paths]
# Path to where grafana can store temp files, sessions, and the sqlite3 db (if that is used)
data = /home/git/grafana

[server ]
# Protocol (http or https)
protocol  = http

# The http port  to use
http_port = 9999

# Redirect to correct domain if host header does not match domain
# Prevents DNS rebinding attacks
enforce_domain = true
```

则程序可以这么写
```go
package main

import (
	"fmt"
	creader "practice/ConfigurationReader"//包在$gopath/src下的路径
)
func main() {
	filename := "data.ini"
	var listener creader.MyListener

	cfg, err := creader.Watch(filename, listener)
	if err != nil {
		panic(err)
	}

	// 典型读取操作，默认分区可以使用空字符串表示
	section1, err := cfg.GetSection("")
	if err != nil {
		panic(err)
	}
	fmt.Println("App Mode:",section1.GetValByKey("app_mode"))

	section2, err := cfg.GetSection("paths")
	if err != nil {
		panic(err)
	}
	fmt.Println("Data Path:", section2.GetValByKey("data"))
}

```

输出结果如下：
```bash
xumy@xumy-VirtualBox:~/go_test$ go run test.go
App Mode: developments
Data Path: /home/git/grafana
```