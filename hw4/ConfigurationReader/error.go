package ConfigurationReader

import (
	"fmt"
)

//KeyDoesNotExist 自定义错误，当试图访问一个Section中不存在的Key时返回
// type KeyDoesNotExist  struct {
// 	key string
// }
// func (err KeyDoesNotExist) Error() string {
// 	return fmt.Sprintf("Error: key %s does not exist!", err.key)
// }


//SecNameDoesNotExist 自定义错误，当试图访问一个iniCFG中不存在的Section时返回
type SecNameDoesNotExist  struct {
	secName string
}
func (err SecNameDoesNotExist) Error() string {
	return fmt.Sprintf("Error: section %s does not exist!", err.secName)
}

