package service

import (
	"net/http"
	"github.com/unrolled/render"
)

func login(formatter *render.Render) http.HandlerFunc {

	return func(w http.ResponseWriter, req *http.Request) {
		if req.Method == "GET" {
			formatter.HTML(w, http.StatusOK, "login", struct{}{})
		} else {
			req.ParseForm()
			formatter.HTML(w, http.StatusOK, "after_login", struct{
				Username string 
				Password string
			} {
				Username: req.Form["username"][0],
				Password: req.Form["password"][0],
			})
		}
		
	}
}

