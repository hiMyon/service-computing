package cmd

import (
	"fmt"
	"time"
	//"github.com/spf13/cobra"
	cobra "practice/myCobra"
)

var (
	timeCmd = &cobra.Command{
		Use:   "time",
		Short: "get current time",
		Long: `subcommand of myapp, return current time`,
		Run: func(cmd *cobra.Command, args []string) {
			// Do Stuff Here
			fmt.Println(time.Now())
		},
	  }
)
  
