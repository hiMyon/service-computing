package cmd

import (
	"fmt"
	"os"
	//flag "github.com/spf13/pflag"
	//"github.com/spf13/cobra"
	cobra "practice/myCobra"
)

var (
	rootCmd = &cobra.Command{
		Use:   "myapp",
		Short: "a very nice app",
		Long: `A very nice app built with
						love by spf13 and friends in Go.
						Complete documentation is available at http://hugo.spf13.com`,
		Run: func(cmd *cobra.Command, args []string) {
			// Do Stuff Here
			
			fmt.Printf("running myapp command")
			if (author != "YOUR NAME") {
				fmt.Printf(" created by %s.", author)
			}
			fmt.Println()
		},
	}

	author string

)

func init() {
	rootCmd.PersistentFlags().StringVarP(&author, "author", "a", "YOUR NAME", "author name for copyright attribution")
	rootCmd.AddCommand(timeCmd)
	rootCmd.AddCommand(selpgCmd)
}
  
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

