package myCobra

import (
	"testing"
	"fmt"
	"time"
	"os"
	//"github.com/spf13/cobra"
	
)

var (
	timeCmd = &Command{
		Use:   "time",
		Short: "get current time",
		Long: `subcommand of myapp, return current time`,
		Run: func(cmd *Command, args []string) {
			// Do Stuff Here
			fmt.Println(time.Now())
		},
	}
	rootCmd = &Command{
		Use:   "myapp",
		Short: "a very nice app",
		Long: `A very nice app built with
						love by spf13 and friends in Go.
						Complete documentation is available at http://hugo.spf13.com`,
		Run: func(cmd *Command, args []string) {
			// Do Stuff Here
			
			fmt.Printf("running myapp command created by %s.\n", author)
		},
	}

	author string
)


func init() {
	rootCmd.PersistentFlags().StringVarP(&author, "author", "a", "YOUR NAME", "author name for copyright attribution")
	rootCmd.AddCommand(timeCmd)
}
  
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func TestParseArgs(t *testing.T) {
	var args []string
	args = []string {}
	ParseArgs(rootCmd, args)
	if rootCmd.args != nil || timeCmd.args != nil {
		t.Errorf("expected: rootCmd.args = %v, timeCmd.args = %v\n", nil, nil)
		t.Errorf("got:     rootCmd.args = %v, timeCmd.args = %v", rootCmd.args, timeCmd.args)
	}

	args = []string {"time", "-a"}
	ParseArgs(rootCmd, args)
	if len(rootCmd.args) != 1 || rootCmd.args[0] != "time" || len(timeCmd.args) != 1 || timeCmd.args[0] != "-a" {
		t.Errorf("expected: rootCmd.args = %v, timeCmd.args = %v\n", []string{"time"}, []string{"-a"})
		t.Errorf("got:     rootCmd.args = %v, timeCmd.args = %v", rootCmd.args, timeCmd.args)
	}
 	
}

func TestAddCommand(t *testing.T) {
	randCmd := &Command{
		Use:   "rand",
		Short: "generate random integer",
		Long: `generate random integer`,
		Run: func(cmd *Command, args []string) {
			// Do Stuff Here
			
			fmt.Printf("Generate 1\n")
		},
	}
	for _, v := range  rootCmd.commands {
		if v == randCmd {
			t.Errorf("expected: %v\n", []*Command {timeCmd})
			t.Errorf("got:      %v\n", rootCmd.commands)
		}
	}
	rootCmd.AddCommand(randCmd)
	for _, v := range rootCmd.commands {
		if v == randCmd {
			return
		}
	}
	t.Errorf("expected: %v\n", []*Command {timeCmd, randCmd})
	t.Errorf("got:      %v\n", rootCmd.commands)
}

func TestName(t *testing.T) {
	name := rootCmd.Name()
	if name != "myapp" {
		t.Errorf("expected: %s", "myapp")
		t.Errorf("got:      %s", name)
	}
}