### myCobra 
这是一个模仿[cobra](https://github.com/spf13/cobra)写出的低配版库，提供了一些简单接口，方便用户开发具有多级命令的命令行程序。

详细地实现介绍请见博客[https://blog.csdn.net/weixin_43867940/article/details/109249393](https://blog.csdn.net/weixin_43867940/article/details/109249393)

以下是使用这个库开发简单命令行程序的一个例子，例子中用到的方法在cobra中都有同名的方法，且功能相近，你可以按使用cobra库的方法来使用它。

#### 一、安装包
`go get -u https://gitee.com/hiMyon/service-computing/tree/master/hw5/myCobra`

然后你可以将myCobra包拷贝到src下任意的位置，我将它放在了practice文件夹中

#### 二、使用包
假设现在要开发一个程序myapp，其下有一个子命令time

建议开发时程序有以下结构：
```bash
▾ myapp/
    ▾ cmd/
        root.go
        time.go
      main.go
```

cmd文件夹中每个文件存放一条命令的定义，root.go定义了根命令。

main.go只做了一件事情：启动程序。可以简单编写如下：
```go
package main

import (
  "practice/myapp/cmd"
)

func main() {
  cmd.Execute()
}
```

注意“practice/myapp”要修改成myapp包的路径

定义根命令：
```go
package cmd

import (
	"fmt"
	"os"
	cobra "practice/myCobra"
)

var rootCmd = &cobra.Command{
	Use:   "myapp",
	Short: "a very nice app",
	Long: `A very nice app built with
				  love by spf13 and friends in Go.
				  Complete documentation is available at http://hugo.spf13.com`,
	Run: func(cmd *cobra.Command, args []string) {
	  // Do Stuff Here
	  
	  fmt.Printf("running myapp command.\n")
	},
}
  
func Execute() {
if err := rootCmd.Execute(); err != nil {
    fmt.Fprintln(os.Stderr, err)
    os.Exit(1)
}
}

```

定义子命令time：
```go
package cmd

import (
	"fmt"
	"time"
	"github.com/spf13/cobra"
)

var (
	timeCmd = &cobra.Command{
		Use:   "time",
		Short: "get current time",
		Long: `subcommand of myapp, return current time`,
		Run: func(cmd *cobra.Command, args []string) {
			// Do Stuff Here
			fmt.Println(time.Now())
		},
	  }
)
```

然后在root.go的init函数中将time加入到myapp的子命令数组中
```go
func init() {
	rootCmd.AddCommand(timeCmd)
}
```

编译和安装myapp

`go install practice/myapp`

运行：
```bash
xumy@xumy-VirtualBox:~/gopath/src/practice/cobraApp/cmd$ myapp
running myapp command.
xumy@xumy-VirtualBox:~/gopath/src/practice/cobraApp/cmd$ myapp time
2020-10-23 21:48:07.13075937 +0800 CST m=+0.001690956
```

#### 三、增加选项

上面的例子中两个命令都不带选项，很是单调，现在我们给myapp加上选项‘作者’

在root.go的init函数中增加此句
```go
rootCmd.PersistentFlags().StringVarP(&author, "author", "a", "YOUR NAME", "author name for copyright attribution")
```

以及修改rootCmd的Run函数，令其输出作者信息。

重新编译安装后运行：
```bash
xumy@xumy-VirtualBox:~/gopath/src/practice/cobraApp/cmd$ myapp -a xumy
running myapp command created by xumy.
```

#### 四、查看帮助
对于任何命令，你都可以通过help获取使用的帮助信息

```bash
xumy@xumy-VirtualBox:~/gopath/src/practice/cobraApp/cmd$ myapp -h
A very nice app built with
						love by spf13 and friends in Go.
						Complete documentation is available at http://hugo.spf13.com

Usage:
  myapp [flags]
  myapp [command]

Available Commands:
  help        Help about any command
  time        get current time

Flags:
  -a, --author string   author name for copyright attribution (default "YOUR NAME")
  -h, --help            help for myapp

Use "myapp [command] --help" for more information about a command.

```



